package com.example.denis.geochat;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Date;
import java.util.List;

public class Map extends AppCompatActivity {
    EditText editText;
    Button send;
    ImageButton sputnik, karta, gibrid;
    GoogleMap map;
    ParseObject server;
    String userName;
    public static double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            userName = null;
        } else {
            userName = extras.getString("name");
        }

        editText = (EditText) findViewById(R.id.editText);
        send = (Button) findViewById(R.id.send);
        sputnik = (ImageButton) findViewById(R.id.sputnik);
        karta = (ImageButton) findViewById(R.id.karta);
        gibrid = (ImageButton) findViewById(R.id.gibrid);

        sputnik.setOnClickListener(onClickListener);
        karta.setOnClickListener(onClickListener);
        gibrid.setOnClickListener(onClickListener);
        send.setOnClickListener(onClickListener);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Messages");
        query.orderByDescending("created").setLimit(20);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                for (ParseObject o : list){
                    Log.d("last20", o.getString("date") + o.getString("username"));
                }

            }
        });


        Intent intent = new Intent(Map.this, MyService.class);
        startService(intent);
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMap();
//        Marker marker = map.addMarker(new MarkerOptions().position().title("")); //create marker
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(, 15));
//        map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.sputnik:
                    map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                    break;
                case R.id.karta:
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    break;
                case R.id.gibrid:
                    map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    break;
                case R.id.send:
                    server = new ParseObject("Messages");
                    server.put("username", userName);
                    server.put("message", editText.getText().toString());
                    server.put("latitude", String.valueOf(lat));
                    server.put("longitude", String.valueOf(lng));
                    server.put("date", DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date(System.currentTimeMillis())));
                    server.saveInBackground();
                    editText.setText("");
                    break;
            }
        }
    };
}
