package com.example.denis.geochat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseObject;

import java.util.Date;

public class Welcome extends AppCompatActivity {
    EditText editName;
    Button start;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    String user;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcom);
        sp = getPreferences(Context.MODE_PRIVATE);
        editor = sp.edit();
        user = sp.getString("name", null);
        intent = new Intent(Welcome.this, Map.class);
        start = (Button) findViewById(R.id.start);
        editName = (EditText) findViewById(R.id.editName);
        editName.addTextChangedListener(new TextListener() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (!editName.getText().toString().equals("")) {
                    start.setEnabled(true);
                } else {
                    start.setEnabled(false);
                }
            }
        });
        start.setOnClickListener(onClickListener);

        String name = sp.getString("name", "Unknown");
        if (user != null) {
            intent.putExtra("name", name);
            startActivity(intent);
        }

    }

    abstract class TextListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = editName.getText().toString();
            if (user == null) {
                intent.putExtra("name", name);
                editor.putString("name", name);
                editor.commit();
                startActivity(intent);
            }
        }
    };
}
